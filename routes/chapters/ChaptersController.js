import {
	getPermissions,
	returnSQLResponse,
} from '../util';

//Gets all chapters from the database
export const getChapters = (request, response, connection) => {

	const permissions = getPermissions(request);

	connection.query(
		`SELECT chapters.*
		FROM chapters
		WHERE chapters.id IN ${permissions.chaptersSQL}
		OR chapters.id IN (
			SELECT chapter_id FROM school_registration WHERE school_id IN ${permissions.schoolsSQL}
		)
		OR ${permissions.hasFullAccess}
		ORDER BY status ASC, name ASC`,
		returnSQLResponse(connection, response)
	);
};


export const getChapter = (request, response, connection) => {
	const {id} = request.params;

	const {
		chaptersSQL,
		schoolsSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT chapters.*
		FROM chapters
		WHERE id in(${id})
		AND (
			id in ${chaptersSQL}
			OR
			id IN (
				SELECT chapter_id FROM school_registration WHERE
					school_id IN ${schoolsSQL}
			)
			OR
			${hasFullAccess}
		)`,
		returnSQLResponse(connection, response)
	);
};
