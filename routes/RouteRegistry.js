import registerChapterRoutes from './chapters';
import registerRallyRoutes from './rallies';
import registerSchoolRoutes from './schools';
import registerWitnessingRoutes from './witnessing';
import registerDashboardRoutes from './dashboard';
import registerAdminRoutes from './admin';
import registerMailRoutes from './mail';
import registerAuthenticationRoutes from './authentication';
import registerAnalyticsRoutes from './analytics';

export default app => {
	registerAuthenticationRoutes(app);
	registerChapterRoutes(app);
	registerRallyRoutes(app);
	registerSchoolRoutes(app);
	registerWitnessingRoutes(app);
	registerDashboardRoutes(app);
	registerAdminRoutes(app);
	registerMailRoutes(app);
	registerAnalyticsRoutes(app);
}