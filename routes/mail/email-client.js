import nodemailer from 'nodemailer';
import format from 'string-template';
import fs from 'fs';
import * as STATUS_CODES from 'http-status-codes';

//set up node mailer

const transporter = nodemailer.createTransport({
	service:'gmail',
	auth: {
		user: 'campusrecords@nationalschoolproject.com',
		pass:'Csp16175'
	}
});

const _sendEmail = (to, subject, data, parameters, resolve, reject, response) => {
	if (!Array.isArray(to)) {
		to = [to];
	}

	to.forEach((value) => {
		const options = {
			from: 'Campus Records',
			value,
			subject: typeof subject === 'string' ? subject : format(subject.template, subject.parameters),
			html: typeof parameters === 'object' ? format(data, parameters) : data,
		};

		transporter.sendMail(options, (error, info) => {
			if (error) {
				if (response) {
					response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(error)
				}
				reject(error);
			} else {
				if (response) {
					response.status(STATUS_CODES.OK).send('OK');
				}
				resolve(info);
			}
		});
	});
}

/**
 * @description Sends an email to a specified recipient using either plain text or a template
 * 
 * @param {(string|string[])} to The recipient or recipients of the email
 * @param {(string|object)} subject The subject of the email, if it is an object then subject.template will be the template and subject.parameters will be used as its parameters
 * @param {string} template The body of the email, if the string begins with '/' then the template will be loaded from the /templates folder
 * @param {Object} parameters The parameters to be inserted into the string
 * @param {Object=} response Express response object, if set, response will be automatically sent on failure or success
 */
export default (to, subject, template, parameters, response) => {
	//Wrap read file in a promise so end user can use Promise.then
	//to handle success and Promise.catch to handle failure
	return new Promise((resolve, reject) => {
		if (template.startsWith('/')) {
			const filePath = require.resolve(`../../email${template}`);
			fs.readFile(filePath, 'utf8', (err, data) => {

				if (err) {
					reject(err);
				}
				_sendEmail(to, subject, data, parameters, resolve, reject, response);
	
			});
		} else {
			_sendEmail(to, subject, template, parameters, resolve, reject, respones);
		}
	});
}