import React from 'react';
import Card from '../card/Card';
import NumberDisplay from '../number-display/NumberDisplay';
import CollapseButton from '../collapse-button/CollapseButton';
import moment from 'moment';
import _ from 'lodash';

export default class SchoolInfoModule extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			collapsed: this.props.collapsed,
			mostRecentEnrollment: "NA",
			mostRecentChapter: "NA",
			timesEnrolled: 0,
			enrolledInRange: false,
		}
	}

	handleCollapseState() {
		this.setState({
			collapsed: !this.state.collapsed
		});
	}

	getMostRecentRegistration = () => {
		let enrolledInRange = false;
		let minDate = moment(this.props.minDate).valueOf();
		let maxDate = moment(this.props.maxDate).valueOf();

		const reg = _.maxBy(this.props.registrations, (o) => {
			const start = moment(o.start_date).valueOf();
			const end = moment(o.end_date).valueOf();

			if (start >= minDate || end <= maxDate)
				enrolledInRange = true;

			return start;
		});

		if (!reg) {
			return {
				chapter: 'NA',
				date: 'NA',
				enrolledInRange: false,
			}
		}

		return {
			chapter: reg.chapter,
			date: moment(reg.start_date).format("MM/DD/YYYY"),
			enrolledInRange
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			collapsed: nextProps.collapsed
		});
	}

	dateRangeContains(minDate, maxDate, startDate, endDate) {
		let exclude = moment('0000-00-00');
		return !startDate.isSame(exclude) && (startDate.isBetween(minDate, maxDate, 'days', '[]') ||
			endDate.isBetween(minDate, maxDate, 'days', '[]') ||
			minDate.isBetween(startDate, endDate, 'days', '[]') ||
			maxDate.isBetween(startDate, endDate, 'days', '[]'));
	}

	render() {
		let labelStyle = {
			"fontSize": "15px"
		}
		let disStyle = {
			"color": "#EEA00F"
		}
		const mostRecentRegistration = this.getMostRecentRegistration();
		return (
			<Card cssClass={this.props.cssClass}>
				<div className="mdl-grid mdl-grid-centered">
					<div className="mdl-cell">
						<NumberDisplay label="Most Recent Chapter" num_css="importance-4" number_display={mostRecentRegistration.chapter.trim() == "" ? "NA" : mostRecentRegistration.chapter} />
					</div>

					<div className="mdl-cell">
						<NumberDisplay label="State" num_css="importance-4" number_display={this.props.state.trim() == "" ? "NA" : this.props.state} />
					</div>

					<div className="mdl-cell">
						<NumberDisplay label="County" num_css="importance-3" number_display={this.props.county.trim() == "" ? "NA" : this.props.county} />
					</div>
				</div>
				{
					this.props.isLoaded ?
						<div className="mdl-grid">
							<CollapseButton
								collapsed={this.state.collapsed}
								collapsed_label="More"
								onClick={this.handleCollapseState.bind(this)}
								raised={false}
								uncollapsed_label="Less"
							/>
							<br />
						</div>
						: ""
				}
				{
					this.state.collapsed || !this.props.isLoaded ?
						(
							<div className="mdl-grid">
								<div className="mdl-cell mdl-cell--12-col">
									<ul>
										<li><span style={labelStyle}>Address:</span> {this.props.address == "" ? "NA" : <span style={disStyle}><a href={`https://www.google.com/maps/place/${this.props.name}/${this.props.address}`} target="_blank">{this.props.address}</a></span>}</li>
										<li><span style={labelStyle}>Most recent enrollment:</span> <span style={disStyle}>{mostRecentRegistration.date}</span></li>
										<li><span style={labelStyle}>Total enrollments:</span> <span style={disStyle}>{this.props.registrations.length}</span></li>
										<li><span style={labelStyle}>This school </span><span className={mostRecentRegistration.enrolledInRange ? "positive" : "negative"}>{mostRecentRegistration.enrolledInRange ? "is" : "is not"} enrolled</span><span style={labelStyle}> in the current range</span></li>
									</ul>
								</div>
							</div>
						)
						:
						null
				}
			</Card>
		)

	}

}
