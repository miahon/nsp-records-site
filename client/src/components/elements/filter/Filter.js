import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import Card from '../card/Card';
import styles from './styles.css';

/**
 * @description A component used for filtering lists. It's pretty powerful except it's kind of ugly
 */
export default class Filter extends React.Component {

	constructor(props) {
		super(props);

		//first dimension is 'or's and second dimension is 'and's
		this.state = {
			filters: [
				[
					{
						colIndex: 0,
						value: "",
						opIndex: 0,
						type: this.props.columns[0].type
					}
				]
			]
		}

		this.operators = [
			{
				display: "equals",
				op: (c1, c2) => {
					return c1 == c2;
				},
				types: [
					"number",
					"string"
				]
			},
			{
				display: "does not equal",
				op: (c1, c2) => {
					return c1 != c2;
				},
				types: [
					"number",
					"string"
				]
			},
			{
				display: "is greater than",
				op: (c1, c2) => {
					return c1 > c2;
				},
				types: [
					"number"
				]
			},
			{
				display: "is less than",
				op: (c1, c2) => {
					return c1 < c2;
				},
				types: [
					"number"
				]
			},
			{
				display: "contains",
				op: (c1, c2) => {
					if (typeof c1 == "string" && typeof c2 == "string") {
						return c1.indexOf(c2) >= 0
					} else {
						return false;
					}
				},
				types: [
					"string"
				]
			},
			{
				display: "does not contain",
				op: (c1, c2) => {
					if (typeof c1 == "string" && typeof c2 == "string") {
						return c1.indexOf(c2) == -1;
					} else {
						return false;
					}
				},
				types: [
					"string"
				]
			},
			{
				display: "is on",
				op: (c1, c2) => {
					let d1 = moment(c1);
					let d2 = moment(c2);
					return d1 == d2;
				},
				types: [
					"date"
				]
			},
			{
				display: "is after",
				op: (c1, c2) => {
					let d1 = moment(c1);
					let d2 = moment(c2);
					return d1 > d2;
				},
				types: [
					"date"
				]
			},
			{
				display: "is before",
				op: (c1, c2) => {
					let d1 = moment(c1);
					let d2 = moment(c2);
					return d1 < d2;
				},
				types: [
					"date"
				]
			},
			{
				display: "is longer than",
				op: (c1, c2) => {
					return c1.length > c2
				},
				types: [
					"string"
				]
			},
			{
				display: "is shorter than",
				op: (c1, c2) => {
					return c1.length < c2
				},
				types: [
					""
				]
			},
			{
				display: "is not blank",
				op: (c1, c2) => {
					return c1 != "" && c1 != null && c1 != undefined
				},
				types: [
					"boolean",
					"number",
					"date",
					"string"
				]
			},
		]
	}

	componentDidUpdate() {
		componentHandler.upgradeDom();
	}

	changeFilterValue(i1, i2, event) {
		this.state.filters[i1][i2].value = event.target.value;
		this.setState({
			filters: this.state.filters
		})
	}

	changeFilterOperation(i1, i2, event) {
		this.state.filters[i1][i2].opIndex = event.target.value;
		this.setState({
			filters: this.state.filters
		})
	}

	changeFilterColumn(i1, i2, event) {
		this.state.filters[i1][i2].colIndex = event.target.value;
		this.state.filters[i1][i2].type = this.props.columns[event.target.value].type;
		this.state.filters[i1][i2].opIndex = 0;
		this.setState({
			filters: this.state.filters,
		})
	}

	andClicked(orGroup) {
		this.state.filters[orGroup].push({
			colIndex: 0,
			value: "",
			opIndex: 0,
			type: this.props.columns[0].type
		})
		this.setState({
			filters: this.state.filters
		})
	}

	removeClicked(orIndex, index) {
		this.state.filters[orIndex].splice(index, 1);
		this.setState({
			filters: this.state.filters
		})
	}

	removeOrClicked(orIndex) {
		this.state.filters.splice(orIndex + 1, 1);
		this.setState({
			filters: this.state.filters
		})
	}

	orClicked() {
		this.state.filters.push([
			{
				colIndex: 0,
				value: "",
				opIndex: 0,
				type: this.props.columns[0].type
			}
		])
		this.setState({
			filters: this.state.filters
		})
	}

	validateRow(row) {
		for (let i = 0; i < this.state.filters.length; i++) {
			let and = true;
			for (let j = 0; j < this.state.filters[i].length; j++) {
				let column = this.props.columns[this.state.filters[i][j].colIndex].column;
				if (!this.operators[this.state.filters[i][j].opIndex].op(typeof column == "function" ? column(row) : row[column], this.state.filters[i][j].value)) {
					and = false;
					break;
				}
			}
			if (and) {
				return true;
			}
		}
		return false;
	}

	filter() {
		let results = _.filter(this.props.data, this.validateRow.bind(this));
		let obj = {}
		obj[this.props.resultsField] = results
		this.props.setState(obj);
	}

	clearFilter() {
		let obj = {}
		obj[this.props.resultsField] = this.props.data;
		this.props.setState(obj);
		this.setState({
			filters: [
				[
					{
						colIndex: 0,
						value: "",
						opIndex: 0,
						type: this.props.columns[0].type
					}
				]
			]
		})
	}

	render() {
		if (this.props.showing)
			return (
				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--12-col">

						<Card useSystemCard={true}>
							<h4>Only show me events where</h4>
							{
								this.state.filters.length <= 0 ?
									<button className="mdl-button mdl-js-button mdl-button--fab">
										<i className="material-icons">add</i>
									</button>
									:
									<table className={styles.filterTable}>
										<tbody>
											{
												this.state.filters.map(
													(orGroup, orIndex) => {
														let rval = orGroup.map(
															(filter, index) => {
																return <tr>
																	<td>
																		<select onChange={(event) => this.changeFilterColumn(orIndex, index, event)} value={filter.colIndex} className={styles.filterTableElement}>
																			{
																				this.props.columns.map(
																					(column, colIndex) => {
																						return <option value={colIndex}>{column.display}</option>
																					}
																				)
																			}
																		</select>
																	</td>
																	<td>
																		<select value={filter.opIndex} onChange={(event) => this.changeFilterOperation(orIndex, index, event)} className={styles.filterTableElement}>
																			{
																				this.operators.map(
																					(op, opIndex) => {
																						if (op.types.includes(this.state.filters[orIndex][index].type))
																							return <option value={opIndex}>{op.display}</option>
																					}
																				)
																			}
																		</select>
																	</td>
																	<td>
																		<div className="mdl-textfield mdl-js-textfield">
																			<input onChange={(event) => this.changeFilterValue(orIndex, index, event)} className="mdl-textfield__input" type="text" id="value" value={filter.value} />
																			<label className="mdl-textfield__label" htmlFor="value"></label>
																		</div>
																	</td>
																	<td>
																		{
																			index == orGroup.length - 1 ?
																				<button className="mdl-button mdl-js-button mdl-button--raised" onClick={() => this.andClicked(orIndex)}>
																					AND
                                        										</button>
																				:
																				<div>
																					<span>AND</span>
																					<button onClick={() => this.removeClicked(orIndex, index)} className="mdl-button mdl-js-button mdl-button--icon">
																						<i className="material-icons">remove</i>
																					</button>
																				</div>
																		}
																	</td>
																</tr>
															}
														)

														if (orIndex < this.state.filters.length - 1)
															rval.push(<tr>
																<td>
																	<span>OR</span>
																	<button onClick={() => this.removeOrClicked(orIndex)} className="mdl-button mdl-js-button mdl-button--icon">
																		<i className="material-icons">remove</i>
																	</button>
																</td>

															</tr>)
														else {
															rval.push(
																<tr>
																	<td>
																		<button onClick={() => this.orClicked()} className="mdl-button mdl-js-button mdl-button--raised">
																			OR
                                   										</button>
																	</td>
																</tr>
															)
														}
														return rval;
													}
												)
											}
										</tbody>
									</table>
							}

							<button onClick={this.filter.bind(this)} className="mdl-button mdl-js-button mdl-button--icon">
								<i className="material-icons">refresh</i>
							</button>
							<button onClick={this.clearFilter.bind(this)} className="mdl-button mdl-js-button mdl-button--icon">
								<i className="material-icons">clear</i>
							</button>
						</Card>

					</div>
				</div>
			)
		else
			return (
				<div></div>
			)
	}

}
