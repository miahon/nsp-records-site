import React from 'react';
import styles from './databox.css';

/**
 * @description The box view for the ranged ministry results on the main dashboard
 */
export default class DataBox extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			title: props.title,
			titleEdit: false,

			ralliesHover: false,
			witnessingHover: false,
			schoolsHover: false
		}
	}

	toggleHover(attribute) {
		let obj = {}

		obj[attribute] = !this.state[attribute];

		this.setState(obj)
	}

	editTitle(event) {
		this.setState({
			title: event.target.value
		})
		this.props.changeTitle(event.target.value);
	}

	setEditMode(state) {
		this.setState({
			titleEdit: state
		}, () => {
			componentHandler.upgradeDom();
		})
	}

	componentDidMount() {
		componentHandler.upgradeDom();
	}

	componentDidUpdate() {
		componentHandler.upgradeDom();
	}

	render() {
		return (
			<div className={styles.dataBoxContainer}>

				<div className={styles.dataBoxControlButtons}>
					<button id="edit-title" className="mdl-button mdl-js-button mdl-button--icon" onClick={() => this.setEditMode(true)}>
						<i className="material-icons">edit</i>
					</button>
					<div htmlFor="edit-title" className="mdl-tooltip">
						Edit range title
          			</div>
					<button id="delete-range" className="mdl-button mdl-js-button mdl-button--icon" onClick={this.props.onRemove}>
						<i className="material-icons">cancel</i>
					</button>
					<div htmlFor="delete-range" className="mdl-tooltip">
						Delete range
          			</div>
				</div>

				{
					!this.state.titleEdit ?
						<h4 className={styles.dataBoxTitle}>
							{this.state.title}
						</h4>
						:
						<div className={styles.dataBoxTitleEdit}>
							<form onSubmit={(event) => { event.preventDefault(); if (this.state.title != "") this.setEditMode(false) }}>
								<div className="mdl-textfield mdl-js-textfield">
									<input onChange={this.editTitle.bind(this)} className="mdl-textfield__input" type="text" id="titleField" />
									<label className="mdl-textfield__label" htmlFor="titleField">{this.state.title}</label>
								</div>
								<button className="mdl-button mdl-js-button mdl-button--icon">
									<i className="material-icons">check_circle</i>
								</button>
							</form>
						</div>
				}
				<div className={styles.dataBox} onClick={() => this.toggleHover("ralliesHover")}>
					<div className={`${styles.dataBoxContent} ${this.state.ralliesHover ? styles.hover : ""}`}>
						<p className={styles.dataBoxNumberDisplay}>
							{this.props.data.allTimeRallies}
						</p>
						<p className={styles.dataLabel}>
							Total Rallies
            			</p>
						<div className={styles.dataBoxTooltip}>
							<div className={styles.dataBoxTooltipBox}>
								<p className={styles.dataBoxNumberDisplayTooltip}>
									{this.props.data.allTimeRallyAttendance}
								</p>
								<p className={styles.dataLabelDropDown}>
									Total Attendance
               					</p>
							</div>
							<div className={styles.dataBoxTooltipBox}>
								<p className={styles.dataBoxNumberDisplayTooltip}>
									{this.props.data.allTimeRallyIndicatedDecisions}
								</p>
								<p className={styles.dataLabelDropDown}>
									Indicated Decisions
               					</p>
							</div>
						</div>
					</div>
				</div>
				<div className={styles.dataBox} onClick={() => this.toggleHover("witnessingHover")}>
					<div className={`${styles.dataBoxContent} ${this.state.witnessingHover ? styles.hover : ""}`}>
						<p className={styles.dataBoxNumberDisplay}>
							{this.props.data.allTimeWitnessingDays}
						</p>
						<p className={styles.dataLabel}>
							Total Witnessing Events
           			 	</p>
						<div className={styles.dataBoxTooltip}>
							<div className={styles.dataBoxTooltipBox}>
								<p className={styles.dataBoxNumberDisplayTooltip}>
									{this.props.data.allTimePeopleApproached}
								</p>
								<p className={styles.dataLabelDropDown}>
									Approached
                				</p>
							</div>
							<div className={styles.dataBoxTooltipBox}>
								<p className={styles.dataBoxNumberDisplayTooltip}>
									{this.props.data.allTimeGospelPresentations}
								</p>
								<p className={styles.dataLabelDropDown}>
									Gospel Presentations
                				</p>
							</div>
							<div className={styles.dataBoxTooltipBox}>
								<p className={styles.dataBoxNumberDisplayTooltip}>
									{this.props.data.allTimeWitnessingIndicatedDecisions}
								</p>
								<p className={styles.dataLabelDropDown}>
									Indicated Decisions
                				</p>
							</div>
						</div>
					</div>
				</div>
				<div className={styles.dataBox} onClick={() => this.toggleHover("schoolsHover")}>
					<div className={`${styles.dataBoxContent} ${this.state.schoolsHover ? styles.hover : ""}`}>
						<p className={styles.dataBoxNumberDisplay}>
							{this.props.data.allTimeSchoolsCoached}
						</p>
						<p className={styles.dataLabel}>
							Schools Coached
            			</p>
						<div className={styles.dataBoxTooltip}>
							<div className={styles.dataBoxTooltipBox}>
								<p className={styles.dataBoxNumberDisplayTooltip}>
									{this.props.data.allTimeSchoolsActive}
								</p>
								<p className={styles.dataLabelDropDown}>
									Schools Active
                				</p>
							</div>
						</div>
					</div>
				</div>
				<div className={styles.dataBox}>
					<div className={styles.dataBoxContent}>
						<p className={styles.dataBoxNumberDisplay}>
							{this.props.data.allTimeIndicatedDecisions}
						</p>
						<p className={styles.dataLabel}>
							Indicated Decisions
            			</p>
					</div>
				</div>
			</div>
		)
	}

}
