import 'whatwg-fetch';

export default query => {
	return fetch(query, {credentials: 'include'}).then(response => response.json());
}
